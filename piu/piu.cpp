// piu.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "piu.h"

int num_requests;
surveillance_request * requests_to_scan;


//#define MAXPIUTXT (140 * 4 + 1)
//#define MAXCHARS (2 * 140 * 4 + 1) //Assume that all chars need to be escaped

#define MAXPIUTXT (11 + 1)
#define MAXJSON (2*11 + 1)

/*
 * Ensure data integrity so that the JSON buffer is properly JSON escaped
 * and that memory buffer has no chars after its '\0'-terminator
 */
void sanitize(char * buf_mem, char * buf_json)
{
  char * j = buf_json;
  char * m = buf_mem;
  size_t mem_characters = strlen(buf_mem);
  size_t total_characters = 0;

  for (; m - buf_mem < MAXPIUTXT - 1; m++)
  {
    if (total_characters > mem_characters)
    {
      //Sanitize piu-txt chars after the '\0' terminator
      *m = '\0';
    }
    else
    {
      //Escape doublequote and backslash
      if (*m == '"' || *m == '\\')
      {
        //Write escaped chars to output if there's space
        if (j + 2 - buf_json < MAXJSON - 1)
        {
          *(j++) = '\\';
          *(j++) = *m;
          total_characters += 2;
        }
      }
      else if (*i 
      {
        //Write normal chars to output if there's space
        if (j + 1 - buf_json < MAXJSON - 1)
        {
          *(j++) = *m; 
          total_characters += 1;
        }
      }
      else
      {
        //Skip control chars from 0x0 to 0x1F
      }
    }
  }
  if (j - buf_json < MAXJSON)
    *j = '\0';
  else
    buf_json[MAXJSON - 1] = '\0';
}
//
//int serialize(piu * entry, char * out, int maxout)
//{
//    char * o = out;
//    //Assume that all chars need to be escaped
//    char piutxt[MAXSERIALIZED];
//    int piutxtlen;
//    sanitize(entry->piu_text_utf8, piutxtlen, piutxt, &piutxtlen);
//    sprintf(o, "{");
//    o = o + strlen(o);
//    sprintf(o, "piu_id:%d,", entry->piu_id);
//    o = o + strlen(o);
//    sprintf(o, "piu_id_of_repiu:%d,", entry->piu_id_of_repiu);
//    o = o + strlen(o);
//    sprintf(o, "user_id_of_repiu:%d,", entry->user_id_of_repiu);
//    o = o + strlen(o);
//    sprintf(o, "user_id_of_poster:%d,", entry->user_id_of_poster);
//    o = o + strlen(o);
//    sprintf(o, "piu_length:%d,", entry->piu_length);
//    o = o + strlen(o);
//    sprintf(o, "visible_only_to_followers:%d,", entry->visible_only_to_followers);
//    o = o + strlen(o);
//    sprintf(o, "piu:\"%s\"}", piutxt);
//    o = o + strlen(o);
//    return 0;
//}
//
//int match(piu * entry, char * pattern)
//{
//    // Here's a fancy substring finding function without backdoors
//    return 1;
//}
//
//
//#define MAXPIU 1024
//void surveil(piu * entry)
//{
//    char piumsg[MAXPIU];
//    int piulen;
//    bool patterns_match, followers_match, fields_match;
//    piu * pattern_piu;
//    user * pattern_user;
//
//
//    surveillance_request * req = 0;
//    for (int i = 0; i < num_requests; i++)
//    {
//        req = requests_to_scan + i;
//        for (int j = 0; j < req->num_patterns; j++)
//        {
//            pattern_piu = req->piu_patterns + j;
//            pattern_user = req->user_patterns + j;
//
//            // The Piu text in the pattern is a substring of the input Piu�s text
//            patterns_match = true;
//            if (match(entry, pattern_piu->piu_text_utf8) == 0)
//            {
//                patterns_match = false;
//            }
//
//            // All ids_following and ids_blocked in the user pattern are 
//            // followed/blocked by the input Piu�s use
//            followers_match = false;
//            for (int k = 0; !followers_match && k < entry->poster->num_following; k++)
//            {
//                if (pattern_user->user_id == *(entry->poster->ids_following + k))
//                {
//                    followers_match = true;
//                }
//
//            }
//            for (int k = 0; !followers_match && k < entry->poster->num_blocked; k++)
//            {
//                if (pattern_user->user_id == *(entry->poster->ids_blocked + k))
//                {
//                    followers_match = true;
//                }
//            }
//
//            // All of the NONZERO fields in the piu pattern match the 
//            // input Piu. Values set to zero are �don�t care� inputs.
//            fields_match = true;
//            if (pattern_piu->piu_id != 0 &&
//                pattern_piu->piu_id != entry->piu_id)
//                fields_match = false;
//            if (pattern_piu->piu_id_of_repiu != 0 &&
//                pattern_piu->piu_id_of_repiu != entry->piu_id_of_repiu)
//                fields_match = false;
//            if (pattern_piu->user_id_of_repiu != 0 &&
//                pattern_piu->user_id_of_repiu != entry->user_id_of_repiu)
//                fields_match = false;
//            if (pattern_piu->user_id_of_poster != 0 &&
//                pattern_piu->user_id_of_poster != entry->user_id_of_poster)
//                fields_match = false;
//            if (pattern_piu->piu_length != 0 &&
//                pattern_piu->piu_length != entry->piu_length)
//                fields_match = false;
//            if (pattern_piu->visible_only_to_followers != 0 &&
//                pattern_piu->visible_only_to_followers != entry->visible_only_to_followers)
//                fields_match = false;
//
//            if (patterns_match &&
//                followers_match && 
//                fields_match &&
//                serialize(entry, piumsg, MAXPIU) == 0)
//            {
//                piulen = strlen(piumsg);
//                fwrite(piumsg, sizeof(char), piulen, req->write_here);
//            }
//        }
//    }
//}
//
//int preprocess(piu * entry)
//{
//    surveil(entry);
//    printf("Processed entry");
//    return 0;
//}

int _tmain(int argc, _TCHAR* argv[])
{
    char in[MAXPIUTXT];
    char out[MAXCHARS];
    sprintf(in, "Hello\"");
    sanitize(in, out);
    printf("%s\n", in);
    printf("%s\n", out);
    return 0;
}
