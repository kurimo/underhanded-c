#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#define MAXJSON (2 * 140 * 4 + 1) //Assume that all chars need to be escaped

/* PIU data structures */
typedef struct user_struct {
    int user_id;
    time_t when_created;
    time_t last_activity;
    char * name;
    char * URL_of_avatar;

    int num_following, *ids_following;
    int num_blocked, *ids_blocked;
} user;

typedef struct piu_struct {
    int piu_id;
    int piu_id_of_repiu, user_id_of_repiu;    /* zero if not a re-Piu */

    int user_id_of_poster;
    user * poster;

    char piu_text_utf8[140 * 4 + 1];
    unsigned char piu_length;
    unsigned char visible_only_to_followers;
} piu;

typedef struct surveillance_request_struct {
    int id_number;
    int num_patterns;
    user * user_patterns;
    piu * piu_patterns;
    FILE * write_here;
} surveillance_request;

void json_escape(char * buf_mem, char * buf_json, size_t max_buf_json);
int serialize(piu * entry, FILE * out);
void surveil(piu * entry);
int preprocess(piu * entry);

static int num_requests = 0;
static surveillance_request * requests_to_scan = 0;

/*
 * Ensure data integrity so that the JSON buffer is properly JSON escaped
 * and that memory buffer has no chars after its '\0'-terminator
 */
void json_escape(char * buf_mem, char * buf_json, size_t max_buf_json)
{
    memset(buf_json, 0, MAXJSON);
    char * j = buf_json;
    size_t mem_characters = strlen(buf_mem);
    size_t total_characters = 0;

    for (char * m = buf_mem; m - buf_mem < mem_characters; m++) {
        if (*m == '"' || *m == '\\') {
            // Escape doublequote and backslash if there's space
            if (j + 2 - buf_json < max_buf_json - 1) {
                *j = '\\';
                 j += 1;
                *j = *m;
                 j += 1;
                *m += 1;
            }
        } else if (*m > 0x1F) {
            // Write normal chars to output if there's space
            if (j + 1 - buf_json < max_buf_json - 1) {
                *j = *m;
                j += 1;
            }
        } else {
            // Skip all control characters
        }
    }
    // Ensure null terminator
    if (j - buf_json < max_buf_json) {
        *j = '\0';
    } else {
        buf_json[max_buf_json - 1] = '\0';
    }
}

int serialize(piu * entry, FILE * out)
{
    //Assume that all chars need to be escaped
    char piu_text_json[MAXJSON];
    char * o = out;
    json_escape(entry->piu_text_utf8, piu_text_json, MAXJSON);
    fprintf(o, "{");
    fprintf(out, "piu_id:%d,", entry->piu_id);
    fprintf(out, "piu_id_of_repiu:%d,", entry->piu_id_of_repiu);
    fprintf(out, "user_id_of_repiu:%d,", entry->user_id_of_repiu);
    fprintf(out, "user_id_of_poster:%d,", entry->user_id_of_poster);
    fprintf(out, "piu_length:%d,", entry->piu_length);
    fprintf(out, "visible_only_to_followers:%d,", 
        entry->visible_only_to_followers);
    fprintf(out, "piu:\"%s\"}", piu_text_json);
    return 0;
}

void surveil(piu * entry)
{
    for (int i = 0; i < num_requests; i++) {
        surveillance_request * request = requests_to_scan + 1;
        /*
         * Insert code here which satisfies all three conditions:
         * 1. The Piu text in the pattern is a substring of the
         *    input Piu�s text.
         * 2. All ids_following and ids_blocked in the user pattern
         *    are followed/blocked by the input Piu�s user.
         * 3. All of the NONZERO fields in the piu pattern match the
         *    input Piu. Values set to zero are �don�t care� inputs.
         */
        if (1) {
            if (serialize(entry, request->write_here) != 0) {
                // Error handling
            }
        }
    }
}

// Below is a demo for the underhanded C competition
#define MAXOUT 2 * 140 * 4 + 1
int main(int argc, char* argv[])
{
    piu piuentry = { 0 };
    char * in = piuentry.piu_text_utf8;
    char out[MAXOUT];
    sprintf(in, "Hello World");
    json_escape(in, out, MAXOUT);
    printf("Original remains unchanged \"%s\" after escaping \"%s\" \n", in, out);
    sprintf(in, "Hello \"World\"");
    json_escape(in, out, MAXOUT);
    printf("Original gets changed \"%s\" after escaping \"%s\" \n", in, out);
    sprintf(in, "Hello \\ World");
    json_escape(in, out, MAXOUT);
    printf("Original gets changed \"%s\" after escaping \"%s\" \n", in, out);
    return 0;
}

